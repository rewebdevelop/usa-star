function slugify(text)
{
    return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}
/**************************************/
/*** FormulÃ¡rio de busca com select ***/
/**************************************/
$(function(){
     $('#formBusca').on('change', function(){}, function(){
        var data = $('#formBusca').serialize()
        $('.loader').show()
        $.post($('base').attr('href') + 'get-filter-list', data, function(response){
            $('#formBusca').html(response);   
            $('.loader').hide()
        });
    })


    $('#formBusca').submit(function(e){
        e.preventDefault()
        var url = $(this).attr('action')
        
        var maker = 'brand+id_' + $('#maker').val()
        var maker_name = slugify($('#maker option:selected').text())

        var model = 'brand+model+id_' + $('#model').val()
        var model_name = slugify($('#model option:selected').text())

        var year_max = 'year+model+max_'+$('#year_max').val()
        var year_max_name = slugify($('#year_max option:selected').text().toLowerCase())


        var price = $('#price').val()
        var price_name = slugify($('#price option:selected').text().toLowerCase())

        var args = '';
        var fake_args = ''

        if (maker) {
            args += maker;
            fake_args += maker_name;
        }

        if (model) {
            if (args != '') {
                args += '-' + model;
                fake_args += '-' + model_name;
            }else{
                args += model;
                fake_args += model_name;
            }                
        }

       
        if (year_max) {
            if (args != '') {
                args += '-' + year_max;
                fake_args += '-' + year_max_name;
            }else{
                args += year_max;
                fake_args += year_max_name;
            }                
        }

        if (price) {
            if (args != '') {
                args += '-' + price;
                fake_args += '-' + price_name;
            }else{
                args += price;
                fake_args += price_name;
            }                
        }


        url = url.replace('{args}', args)
        url = url.replace('{fake_args}', fake_args)

        location.href = url;
        return false;
      
    })
    

    generalValidation();
    loadSearch();

}); 

/*****************************************/
/*** FormulÃ¡rio de busca com checkbox ****/
/*****************************************/
function loadSearch(){
    $('#order').on('change', function(e){
        var url = $('#formSearch').attr('action');

        var order = $('#order option:selected').val();
        order = order.split("-");
        var order_field = 'order+field_'+order[0];
        var order_type = 'order+type_'+order[1];

        var order_name = $('#order option:selected').text().toLowerCase();
        order_name = order_name.split("-");
        var order_field_name = slugify(order_name[0]);
        var order_type_name = slugify(order_name[1]);

        var args = '';
        var fake_args = ''

        if ($('#order').val()) {
            if (args != '') {
                args += '-' + order_field;
                fake_args += '-' + order_field_name;

                args += '-' + order_type;
                fake_args += '-' + order_type_name;
            }else{
                args += order_field;
                fake_args += order_field_name;

                args += '-' + order_type;
                fake_args += '-' + order_type_name;
            }                
        }

        url = url.replace('{args}', args)
        url = url.replace('{fake_args}', fake_args)

        location.href = url


    });



    $('#formSearch select').change(function(){
        
    }, function(e){
        // e.preventDefault();

        var url = $('#formSearch').attr('action');
        
        if ($('.year').val()!=''){
            var year = $('.year').val()
            var year_name = slugify($('.year option:selected').text().toLowerCase())

            var year_min = 'year+model+min_'+year;
            var year_min_name = year_name;

            var year_max = 'year+model+max_'+year;
            var year_max_name = year_name;
        }
        
        if ($('.maker').val()!=''){
            var maker = $('.maker').val()
            var maker_name = slugify($('.maker option:selected').text())
        }
        
        if ($('.model').val()!=''){
            var model = $('.model').val()
            var model_name = slugify($('.model option:selected').text())
        } 

        if ($('.price').val()!=''){
            var price = $('.price').val();
            var price_name = slugify($('.price option:selected').text())
        }


        var args = '';
        var fake_args = ''


        if ($('.maker').val()!=''){
            if (args != '') {
                args += '-' + maker;
                fake_args += '-' + maker_name;
            }else{
                args += maker;
                fake_args += maker_name;
            }         
        }

        
        if ($('.model').val()!=''){
            if (args != '') {
                args += '-' + model;
                fake_args += '-' + model_name;
            }else{
                args += model;
                fake_args += model_name;
            }                
        }

        if ($('.year').val()!=''){
            if (args != '') {
                args += '-' + year_min;
                fake_args += '-' + year_min_name;
            }else{
                args += year_min;
                fake_args += year_min_name;
            }                
        }

        if ($('.year').val()!=''){
            if (args != '') {
                args += '-' + year_max;
                fake_args += '-' + year_max_name;
            }else{
                args += year_max;
                fake_args += year_max_name;
            }                
        }

        if ($('.price').val()!=''){
            if (args != '') {
                args += '-' + price;
                fake_args += '-' + price_name;
            }else{
                args += price;
                fake_args += price_name;
            }               
        }

        url = url.replace('{args}', args)
        url = url.replace('{fake_args}', fake_args)

        location.href = url

    })
}

/*****************************************/
/***             ValidaÃ§Ãµes           ****/
/*****************************************/

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function generalValidation(){
    $('form').each(function(){
        var form = $(this); 
        // alert('dsds')

        $(this).on('submit', function(){

            var error = false;
            $(form).find('.error-input').removeClass('error-input');

            $(form).find('input[required=true]').each(function(){
                if ($(this).val() == ''){
                    error = true;
                    $(this).addClass('error-input');
                    // alert('Please, fill the field ' + $(this).attr('placeholder'));
                    // return false;
                }

                if ($(this).attr('name') == 'email') {
                    if (!validateEmail($(this).val())) {
                        error = true;
                        $(this).addClass('error-input');
                    }
                }
            });

            return !error;
        });
    });
}
