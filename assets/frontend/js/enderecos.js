function initialize() {

  // Exibir mapa;
  var myLatlng = new google.maps.LatLng(-9.64963, -35.733858);
  var mapOptions = {
    zoom: 17,
    center: myLatlng,
    panControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }

  // Parâmetros do texto que será exibido no clique;
  var contentString = '<img src="assets/frontend/images/pinmap.png" style="float:left;margin-right:10px;width:39px;margin-top: 13px;">'+'<h2 style="font-size: 22px;">Auto Premium</h2>' +
  '<p style="font-size: 14px;width: 291px;">Avenida Fernandes Lima, 139 - Maceió/AL.</p>';
  var infowindow = new google.maps.InfoWindow({
      content: contentString,
      maxWidth: 700
  });


  // Exibir o mapa na div #mapa;
  var map = new google.maps.Map(document.getElementById("mapa"), mapOptions);


  // Marcador personalizado;
  var image = 'assets/frontend/images/pinmap.png';
  var marcadorPersonalizado = new google.maps.Marker({
      position: myLatlng,
      map: map,
      icon: image,
      title: 'Avenida Fernandes Lima, 139 - Farol, Maceió - AL, 57055-000.',
      animation: google.maps.Animation.BOUNCE
  });


//   // Exibir texto ao clicar no pin;
  google.maps.event.addListener(marcadorPersonalizado, 'click', function() {
    infowindow.open(map,marcadorPersonalizado);
  });

}


// Função para carregamento assíncrono
function loadScript() {
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyDaWXlxI4GKzzhZY7By5zRVQNdkJFW1kgE&sensor=true_or_false&callback=initialize";
  document.body.appendChild(script);
}

window.onload = loadScript;