<?php
define('THEME_NAME', "Dara-Prime-V2");
define('CLIENT_KEY', "044942de00692569db0f18fc30a0cbb0");
define('SECRET_KEY', "z3dpoByS063HtXAIK6UqXGBDqycEj7Ps");
//define('INTEGRATOR_KEY', "wIiSFpw4NKsEMvLIC5h636ti");
define('INTEGRATOR_KEY', "X4DZ7zj11fSCSfBU0SAxUo3n");
define('SMS_KEY', '6c3fe78695d79c5c317baf71b145e9f0');

if($_SERVER['HTTP_HOST'] == 'localhost'){
   define('ROOT_URL', "http://localhost/usa-star/");
}elseif($_SERVER['HTTP_HOST'] == 'interface.reweb.com.br'){
   define('ROOT_URL', "http://interface.reweb.com.br/html/GrupoEuroAmricaUSAStar/preview/");
}else{
   define('ROOT_URL', "http://usastar.com.br/");
}

define('DOCUMENT_ROOT', __DIR__);
define('CACHE_HOURS', 24);
define('EDIT_MODE', false);
define('CONTACT_EMAIL', "");
define('DEBUG_MODE', false);
define('URL_COMPLEMENT', "em-barra-da-tijuca-rj");
define('URL_NEW_CARS', "veiculos-novos-0km");
define('URL_USED_CARS', "veiculos-seminovos-usados");
define('URL_USED_CARS_DETAIL', "veiculos-seminovos-usados");
define('URL_SERVED_AREAS', 'regioes-atendidas-em-barra-da-tijuca-rj');
define('URL_SERVED_AREAS_DETAIL', 'regiao-atendida-em-barra-da-tijuca-rj');

$apiBase = "http://dealers.rewebmkt.com/api/";
$crmBase = "http://crm2.reweb.com.br/api/websites/";

$ENDPOINTS = array(
    'banners' => $crmBase . 'banners/',
    'highlights' => $crmBase . 'highlights/',
    'cities' => $crmBase . 'cities/',
    'stores' => $crmBase . 'stores/',
    'news' => $crmBase . 'news/',
    'blog_car' => $crmBase . 'news_car/',
    'promotions' => $crmBase . 'promotions/',
    'register_coment' => $crmBase .'register_coment/',
    'categories' => $crmBase . 'news_categories/',
    'category_news' => $crmBase . 'category_news/',
    'new_cars' => $crmBase . 'cars/',
    'translations' => $apiBase . 'client/site/languages',
    'forms' => $apiBase . 'form/',
    'menu' => $apiBase . 'client/site/menu',
    'page' => $apiBase . 'client/site/page/',
    'car_categories' => $apiBase . 'client/cars/categories',
    'car_detail' => $crmBase . 'cars/',
    'files' => $apiBase . 'client/site/files',
    'images' => $apiBase . 'client/site/images',
    'saveContentText' => $apiBase . 'client/site/saveTranslationText',
    'pages' => $apiBase . 'client/site/pages',
    'savePage' => $apiBase . 'client/site/savePage',
    'consortiums' => $crmBase . 'consortiums/',
    'products' => $crmBase . 'products/',
    'products_categories' => $crmBase . 'products_categories/',
    'products_search' => $crmBase . 'products_search/',
);
