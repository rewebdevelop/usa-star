<?php
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/config.php';
require_once __DIR__.'/functions.php';
require_once __DIR__.'/Integrator.php';
require_once __DIR__.'/Mobile_Detect.php';
session_start();

ini_set('display_errors', '0');
//$httphost = $_SERVER['HTTP_HOST'];
//$_SERVER['HTTP_HOST'] = 'www.usastar.com.br';
require_once __DIR__.'/AdManagerAPI.class.php';
$admanager = new AdManagerAPI();
$admanager->registraAcesso();
//$_SERVER['HTTP_HOST'] = $httphost;

//ini_set('display_errors', '1');
error_reporting(E_ALL);

$app = new Silex\Application();
$app['debug'] = DEBUG_MODE;

$mobile = new Mobile_Detect;

$viewsFolder = '/views/';
//if ($mobile->isMobile()) {
//    $viewsFolder = '/views/mobile/';
//}

/*
    Register translation system
*/
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallbacks' => array('pt-br'),
));

/*
    Register TWIG template view system
*/
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.$viewsFolder,
));

/*
    Register HTTP cache
*/
$app->register(new Silex\Provider\HttpCacheServiceProvider(), array(
    'http_cache.cache_dir' => __DIR__.'/cache/',
));


$translations = getRequest($ENDPOINTS['translations'], $app['debug']);
$messages = array();
$defaultLang = '';
foreach ($translations as $lang => $words) {
    // $dict = array();
    if ($defaultLang == '') {
        $defaultLang = $lang;
    }

    foreach ($words as $word) {
        foreach((array)$word as $key => $value){
            $messages[$lang][$key] = $value;
        }
    }
}

// load custom files and images

$files = getRequest($ENDPOINTS['files'], $app['debug']);
foreach ($files as $file) {
    $assetCustomDir = __DIR__.'/assets/custom/';
    if (!empty($file->file_name)) {
        file_put_contents($assetCustomDir . $file->file_name, $file->content);
    }
}

$images = getRequest($ENDPOINTS['images'], $app['debug']);
foreach ($images as $image) {
    $assetCustomDir = __DIR__.'/assets/custom/img/';
    if (!empty($image->file) && !empty($image->content)) {
        if (!file_exists($assetCustomDir . $image->file)) {
            file_put_contents($assetCustomDir . $image->file, base64_decode($image->content));
        }
    }
}


$app['translator.domains'] = array(
    'messages' => $messages
);

/*
    CREATE THE FORM FILTER
*/
$app['twig'] = $app->share($app->extend('twig', function($twig, $app) use($ENDPOINTS, $defaultLang) {
    $filter = new Twig_SimpleFilter('makeform', function ($formname) use($ENDPOINTS, $defaultLang) {
        $lang = $defaultLang;
        $form = getRequest($ENDPOINTS['forms'] . $formname . '/' .  $lang);

        if (isset($form->html)) {
            $form = str_replace('{{ rooturl }}/', ROOT_URL, $form->html);
            return $form;
        }else{
            return $form->error;
        }
    });

    $slug_filter = new Twig_SimpleFilter('slugfy', function ($string) {
        return slugify($string);
    });

    $new_car_url = new Twig_SimpleFilter('url', function ($newCar) {
        return ROOT_URL . slugify($newCar->maker->name) .'/'. URL_NEW_CARS . (URL_NEW_CARS ? '-' : '') . $newCar->slug . '-' . URL_COMPLEMENT;
    });

    $detail_served_areas_url = new Twig_SimpleFilter('detail_served_areas_url', function ($city_slug) {
        return ROOT_URL . URL_SERVED_AREAS_DETAIL . (URL_SERVED_AREAS_DETAIL ? '-' : '') . $city_slug . '-rio-de-janeiro-rj';
    });

    $used_car_url = new Twig_SimpleFilter('used_url', function ($usedCar) {
        return ROOT_URL . URL_USED_CARS_DETAIL . '-' . slugify($usedCar->brand) . '-' . slugify($usedCar->title) . '-cod_' . $usedCar->id;
    });

    $twig->addFilter($slug_filter);
    $twig->addFilter($filter);
    $twig->addFilter($new_car_url);
    $twig->addFilter($used_car_url);
    $twig->addFilter($detail_served_areas_url);
    return $twig;
}));

/*
    Default page - Home
*/
$app->get('/', function() use($app, $defaultLang, $ENDPOINTS){
    $data = defaultData($app);

    $admanager = new AdManagerAPI();
    //$data['unidades'] = $admanager->getUnidadesCliente('www.usastar.com.br');

    $maker = slugify($maker);
    $data['makers'] = $data['makersCars']['makers'];
    $data['maker'] = $maker;


    return $app['twig']->render('index_makers.html', $data);

});

$app->get('/{maker}/' , function($maker) use($app, $ENDPOINTS){

    $data = defaultData($app);

    $admanager = new AdManagerAPI();
    //$data['unidades'] = $admanager->getUnidadesCliente('www.usastar.com.br');

    $integrator = new \Integrator\API(INTEGRATOR_KEY);

    $maker = slugify($maker);
    $_SESSION['maker'] = $maker;

    foreach ($data['makersCars']['makers'] as $makerData) {
        if(slugify($makerData->name) == $maker)
            $maker_id = $makerData->id;
    }

    $banners = getRequest($ENDPOINTS['banners'], $app['debug'], 0.5);
    $banner = array();
    foreach($banners->banners as $key => $row){
        $banner[$key] = $row->ordem;
    }
    array_multisort($banner,SORT_ASC,$banners->banners);
    $data['banners'] = $banners; 
    
    $data['cars']->cars = $data['makersCars']['cars'][$maker_id];
    $data['maker'] = $maker;
    $data['stores'] = getRequest($ENDPOINTS['stores'], $app['debug']);
    $data['highlights'] = getRequest($ENDPOINTS['highlights'], $app['debug']);
    $data['filters'] = $integrator->getFilters();
    $news = getRequest($ENDPOINTS['blog_car'] . $data['car']->id, $app['debug']);
    $data['news'] = $news->news;
    $data['news_recents'] = $news->recents;

    $data['page'] = 'home';
    if (isset($_SESSION['success'])) {
        $data['success'] = $_SESSION['success'];
        unset($_SESSION['success']);
    }

    $data['used_cars_form'] = searchUsedCarsHome(array(), $app, $ENDPOINTS);
    $data['used_cars'] = $integrator->getCars(array('limit' => 4));


    return $app['twig']->render('index.html', $data);
});



$app->get('/blog', function() use($app, $ENDPOINTS){
    $data = defaultData($app);

    $admanager = new AdManagerAPI();
    //$data['unidades'] = $admanager->getUnidadesCliente('www.usastar.com.br');

    $news = getRequest($ENDPOINTS['news'],'news', $app['debug']);
    $data['news'] = array_chunk($news->news, 5);
    $data['news_recents'] = $news->recents;
    $data['news_more_views'] = $news->more_views;
    $data['categories_news'] = getRequest($ENDPOINTS['categories'], $app['debug']);


    return $app['twig']->render('blog.html', $data);
});

$app->get('/blog/{new_slug}', function($new_slug) use($app, $ENDPOINTS){
    $data = defaultData($app);

    $admanager = new AdManagerAPI();
    //$data['unidades'] = $admanager->getUnidadesCliente('www.usastar.com.br');


    $news = getRequest($ENDPOINTS['news'],'news', $app['debug']);
    $data['news'] = $news->news;
    $data['news_recents'] = $news->recents;
    $data['news_more_views'] = $news->more_views;

    $data['new'] = getRequest($ENDPOINTS['news'].$new_slug, $app['debug']);
    $data['categories_news'] = getRequest($ENDPOINTS['categories'], $app['debug']);


    return $app['twig']->render('blog_detalhe.html', $data);

});

$app->post('/register_coment/{new}/{new_slug}', function($new, $new_slug) use($app, $ENDPOINTS){
    $data = Array(
        'name'    => $_POST['name'],
        'email'   => $_POST['email'],
        'message' => $_POST['message'],
        'new_id'  => $new
    );

    postRequest($ENDPOINTS['register_coment'], $data);
    return $app->redirect(ROOT_URL.'blog/'.$new_slug);
});

$app->get('/blog/category/{category_slug}', function($category_slug) use($app, $ENDPOINTS){
    $data = defaultData($app);
    $data['categories_news'] = getRequest($ENDPOINTS['categories'], $app['debug']);
    $news = getRequest($ENDPOINTS['category_news'].$category_slug,'news', $app['debug']);
    $data['news'] = array_chunk($news->news, 5);

    $news_geral = getRequest($ENDPOINTS['news'],'news');
    $data['news_recents'] = $news_geral->recents;
    $data['news_more_views'] = $news_geral->more_views;


    return $app['twig']->render('blog.html', $data);
});

$app->get('/{maker}/' . URL_SERVED_AREAS, function($maker) use($app, $ENDPOINTS){
    $data = defaultData($app);

    $admanager = new AdManagerAPI();
    //$data['unidades'] = $admanager->getUnidadesCliente('www.usastar.com.br');

    $cities = getRequest($ENDPOINTS['cities'], $app['debug']);
    $data['cities'] = $cities->cities;

    $maker = slugify($maker);
    $_SESSION['maker'] = $maker;

    foreach ($data['makersCars']['makers'] as $makerData) {
        if(slugify($makerData->name) == $maker)
            $maker_id = $makerData->id;
    }

    $data['cars']->cars = $data['makersCars']['cars'][$maker_id];
    $data['maker'] = $maker;
    $data['banners'] = getRequest($ENDPOINTS['banners'], $app['debug']);


    return $app['twig']->render('served_areas.html', $data);

});

$app->get('/' . URL_SERVED_AREAS_DETAIL . '-{city_slug}', function($city_slug) use($app, $ENDPOINTS){
    $data = defaultData($app);
    $data['city'] = getRequest($ENDPOINTS['cities'] . $city_slug, $app['debug']);


    return $app['twig']->render('served_areas_detail.html', $data);

})->assert('city_slug', '.*');


function searchUsedCars($args, $app, $ENDPOINTS){
    $data = defaultData($app);
    $integrator = new \Integrator\API(INTEGRATOR_KEY);

    $data['attrs'] = $integrator->getAllAttrs();
    $data['used_cars'] = $integrator->getCars($args);
    $data['filters'] = $integrator->getFilters($args);

    if (!$data['filters']) {
        $data['filters'] = $integrator->getFilters(array());
    }

    if (isset($args['price_min']) && isset($args['price_max'])) {
        $price = $args['price_min'] .'-'.$args['price_max'];
        unset($args['price_min']);
        unset($args['price_max']);
        $args['price'] = $price;
    }

    if (isset($args['odometer_min']) && isset($args['odometer_max'])) {
        $odometer = $args['odometer_min'] .'-'.$args['odometer_max'];
        unset($args['odometer_min']);
        unset($args['odometer_max']);
        $args['odometer'] = $odometer;
    }

    $data['args'] = $args;

    $i = 0;
    if ($data['used_cars']) {
        foreach ($data['used_cars']->data as $car) {
            $data['used_cars']->data[$i]->oldPrice = number_format($car->price + ($car->price*0.15), 2, ',', '.');
            $data['used_cars']->data[$i]->savedPrice = number_format(($car->price*0.2), 2, ',', '.');
            $data['used_cars']->data[$i]->price = number_format($car->price, 2, ',', '.');

            $i++;
        }
    }


    return $app['twig']->render('used_cars.html', $data);
}


function searchUsedCarsHome($args, $app, $ENDPOINTS){
    $integrator = new \Integrator\API(INTEGRATOR_KEY);

    $data['filters'] = $integrator->getFilters($args);
    $data['attrs'] = $integrator->getAllAttrs();
    $data['used_cars'] = $integrator->getCars($args);


    if (!$data['used_cars']) {
        $data['used_cars'] = array();
    }

    $data['args'] = $args;


    $i = 0;
    foreach ($data['used_cars']->data as $car) {
        $data['used_cars']->data[$i]->oldPrice = number_format($car->price + ($car->price*0.2), 2, ',', '.');
        $data['used_cars']->data[$i]->savedPrice = number_format(($car->price*0.2), 2, ',', '.');
        $data['used_cars']->data[$i]->price = number_format($car->price, 2, ',', '.');

        $i++;
    }

    return $data;
}


/*
    USED CAR DETAIL
*/
$app->get('/' . URL_USED_CARS_DETAIL . '-{car_slug}-cod_{car_id}', function($car_slug, $car_id) use($app, $ENDPOINTS){
    $data = defaultData($app);
    $integrator = new \Integrator\API(INTEGRATOR_KEY);
    $data['car'] = $integrator->getCar($car_id);
    $gallery = $data['car']->data->gallery;
    $photo = '';
    foreach ($gallery as $item) {
        if (!$photo) {
            $photo = $item->file;
        }
    }

    $data['photo'] = $photo;
    $data['related_cars'] = $integrator->getCars(array('limit' => 3));

    $data['car']->data->oldPrice = number_format($data['car']->data->price + ($data['car']->data->price*0.2), 2, ',', '.');
    $data['car']->data->savedPrice = number_format(($data['car']->data->price*0.2), 2, ',', '.');
    $data['car']->data->price = number_format($data['car']->data->price, 2, ',', '.');


    return $app['twig']->render('used_car_detail.html', $data);

})->assert('car_slug', '.*')
->assert('car_id', '^\d+$');

$app->get('/' . URL_USED_CARS . '-' . URL_COMPLEMENT, function() use($app, $ENDPOINTS){
    return searchUsedCars(array(), $app, $ENDPOINTS);
});

$app->get('/' . URL_USED_CARS . '-{fake_args}-' . URL_COMPLEMENT . '-{args}', function($args) use($app, $ENDPOINTS){
    return searchUsedCars($args, $app, $ENDPOINTS);
})->assert('args', '.*')
->assert('fake_args', '.*')
->convert('args', function($args){
    $args = explode('-', $args);
    $params = array();

    foreach ($args as $arg) {
        if (count(explode('_', $arg)) > 1) {
            list($name, $value) = explode('_', $arg);
            $params[implode('_', explode('+', $name))] = implode('_', explode('+', $value));
        }
    }

    return $params;
});



$app->get('/{maker}/'. URL_NEW_CARS, function($maker) use($app, $ENDPOINTS){
    $data = defaultData($app);
    $data['categories'] = getRequest($ENDPOINTS['car_categories'], $app['debug']);

   $makers = $data['makersCars']['makers'];
    foreach ($makers as $maker) {
        if($page == slugify($maker->name))
            return $app->redirect(ROOT_URL . $page .'/');
    }
    //debug($makers);
    return $app['twig']->render('new_cars.html', $data);

});

/*
    Default page - Veículos novos - Detalhe
*/
$app->get('/{maker}/' . URL_NEW_CARS . (URL_NEW_CARS ? '-' : '') . '{car_slug}-' . URL_COMPLEMENT, function($maker, $car_slug) use($app, $ENDPOINTS){
    $data = defaultData($app);

    $admanager = new AdManagerAPI();
    //$data['unidades'] = $admanager->getUnidadesCliente('www.usastar.com.br');

    $data['car'] = getRequest($ENDPOINTS['car_detail'] . $car_slug, true);

    $news = getRequest($ENDPOINTS['blog_car'] . $data['car']->id, $app['debug']);
    $data['news'] = $news->news;
    $data['news_recents'] = $news->recents;

    $_SESSION['maker'] = $maker;
    $data['maker'] = $maker;
	 foreach ($data['makersCars']['makers'] as $makerData) {
        if(slugify($makerData->name) == $maker)
            $maker_id = $makerData->id;
    }

    $data['cars']->cars = $data['makersCars']['cars'][$maker_id];

    return $app['twig']->render('car_detail.html', $data);

})->assert('car_slug', '.*');

/*
    Custom pages
*/
$app->get('/{page}', function($page) use($app, $ENDPOINTS, $viewsFolder, $mobile){
    $data = defaultData($app);

    $admanager = new AdManagerAPI();
    //$data['unidades'] = $admanager->getUnidadesCliente('www.usastar.com.br');
    $products = getRequest($ENDPOINTS['products'], $app['debug']);
    $data['products'] = $products->products;
    $data['categories_products'] = getRequest($ENDPOINTS['products_categories'],  $app['debug']);
    $data['banners'] = getRequest($ENDPOINTS['banners'], $app['debug']);

    //debug($data['categories_products']);

    $data['products_total'] = count($products->products);
	$data['promotions'] = getRequest($ENDPOINTS['promotions'], $app['debug']);
    //debug($data['makersCars']);
	//debug($data['promotions']);
    $makers = $data['makersCars']['makers'];
    foreach ($makers as $maker) {
        if($page == slugify($maker->name))
            return $app->redirect(ROOT_URL . $page .'/');
    }

    $page = getRequest($ENDPOINTS['page'] . urlencode($page), $app['debug']);

    if (!isset($page->url)) {
        $data['title'] = 'Página não encontrada';
        $data['message'] = 'A página solicitada não foi encontrada.';

        return $app['twig']->render('error.html', $data);
    }

    if (!file_exists(__DIR__. $viewsFolder . 'temp/')) {
        mkdir(__DIR__. $viewsFolder . 'temp/');
    }

    //if ($mobile->isMobile()) {
    //    $content = $page->content_mobile;
    //}else{
        $content = $page->content;
    //}

    if (!file_exists(__DIR__. $viewsFolder . 'temp/' . $page->url . '.html')) {
        file_put_contents(__DIR__. $viewsFolder . 'temp/' . $page->url . '.html', $content);
    }else if($app['debug']){
        file_put_contents(__DIR__. $viewsFolder . 'temp/' . $page->url . '.html', $content);
    }

    $data['page'] = 'temp/' . $page->url;
    $data['title'] = $page->title;
    $data['keywords'] = $page->keywords;
    $data['description'] = $page->description;

    return $app['twig']->render('page.html', $data);

});


$app->post('/save-text', function() use($app, $ENDPOINTS){
    $content = $_POST['content'];
    $key = $_POST['key'];
    $lang = $_POST['lang'];
    $response = postRequest($ENDPOINTS['saveContentText'], array('content' => $content, 'key' => $key, 'lang' => $lang, 'lang' => $lang, 'lang' => $lang, 'lang' => $lang, 'lang' => $lang));

    return $app->json($response);
});

$app->post('/save-page', function() use($app, $ENDPOINTS){
    $pageId = $_POST['pageId'];
    $content = $_POST['content'];
    $title = $_POST['title'];
    $description = $_POST['description'];
    $keywords = $_POST['keywords'];
    $url = $_POST['url'];
    if (isset($_POST['ssl'])) {
        $ssl = $_POST['ssl'];
    }else{
        $ssl = 0;
    }

    $response = postRequest($ENDPOINTS['savePage'], array('content' => $content,
                                                          'pageId' => $pageId,
                                                          'title' => $title,
                                                          'description' => $description,
                                                          'keywords' => $keywords,
                                                          'url' => $url,
                                                          'ssl' => $ssl));

    return $app->redirect(ROOT_URL . 'cms');
});


$app->post('/send-to-mobile', function() use($app, $ENDPOINTS){
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $url = $_POST['carro'];

    $url = shortURL($url);

    $data = array(
                'sms_token' => SMS_KEY,
                'country_code' => 'BR',
                'message' => 'Ola ' . $name . '! Segue o link do carro no site da Superauto: ' . $url,
                'number' => str_replace(array(' ', ')', '(', '-'), '', $phone)
            );

    $response = postRequest($ENDPOINTS['sendToMobile'], $data);
    $_SESSION['success'] = true;

    return $app->redirect(ROOT_URL);
});


/*

*/
$app->post('/register_lead', function() use($app, $ENDPOINTS){
   $trans = array('name' => 'Nome', 'email' => 'E-mail', 'model'=>'Modelo', 'numero_de_parcelas'=> 'Número de Parcelas', 'valor_da_entrada' => 'Valor Entrada', 'phone' => 'Telefone', 'message' => 'Mensagem', 'city' => 'Cidade', 'state' => "Estado");

    $meio_captacao = $_POST['formCaptation'];
    unset($_POST['formCaptation']);
    $data = $_POST;
    $name = '';
    $email = '';
    $phone = '';
    $city = '';
    $state = '';

    $message = '';
    foreach($data as $field => $value){
        if (isset($trans[$field])) {
            $field = $trans[$field];
        }

        $message .= '<b>' . ucfirst(strtolower($field)) . '</b>' . ': ' . $value . '</br>';
    }

    if (isset($data['name'])) {
        $name = $data['name'];
        unset($data['name']);
    }

    if (isset($data['email'])) {
        $email = $data['email'];
        unset($data['email']);
    }

    if (isset($data['phone'])) {
        $phone = $data['phone'];
        unset($data['phone']);
    }

    if (isset($data['city'])) {
        $city = $data['city'];
        unset($data['city']);
    }

    if (isset($data['state'])) {
        $state = $data['state'];
        unset($data['state']);
    }

    ini_set('display_errors', '0');
    $admanager = new AdManagerAPI();
    //$httphost = $_SERVER['HTTP_HOST'];
    // $_SERVER['HTTP_HOST'] = 'www.usastar.com.br';
    $admanager->registraLead($meio_captacao,$name,$email,$phone,$city,$state,'Brasil',$message);
    // $_SERVER['HTTP_HOST'] = $httphost;

    die('ok');
});


/*
 SEND EMAIL
*/
$app->post('/send_mail', function() use($app, $ENDPOINTS){
    $name = $_POST['name'];
    $phone = $_POST['phone'];

    $message = 'Olá! <br><br>';
    $message .= 'Segue abaixo os dados do usuário:<br>';
    $message .= '<b>Nome: </b>' . $name . '<br>';
    $message .= '<b>Telefone: </b>' . $phone . '<br>';

    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

    if ($name != '' && $phone != ''){
        if (mail(CONTACT_EMAIL, 'Requisição de Ligação', utf8_decode($message), $headers)) {
            return 'ok';
        }else{
            return 'error';
        }
    }

    return 'null';

});

/* enviar trabalhe conosco */
$app->post('/send_mail_job', function() use($app, $ENDPOINTS){
    $data = $_POST;

    $mail = new PHPMailer;
    //$mail->SMTPDebug = 3;                               // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'dealers.reweb@gmail.com';                 // SMTP username
    $mail->Password = 'reweb123';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    $mail->setFrom('dealers.reweb@gmail.com', $data['nome']);
    $mail->addReplyTo($data['email'], $data['nome']);

    $mail->addAddress('rh@italiabarra.com.br'); 
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    $mail->AddAttachment($_FILES['file']['tmp_name'], $_FILES['file']['name']);
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = utf8_decode('Trabalhe Conosco - ' . $data['nome']);
    
    $body = "<h1>" . $data['name'] . "</h1>";
    $body .= "<p><b>E-mail:</b> " . $data['email'] . "</p>";

    if(isset($data['phone']))  $body .= "<p><b>Telefone:</b>          " . $data['phone'] . "</p>";
    if(isset($data['interesse'])) $body .= "<p><b>Área de Interesse:</b> " . $data['interesse'] . "</p>";
    if(isset($data['cidade']))    $body .= "<p><b>Cidade:</b>            " . $data['cidade'] . "</p>";
    if(isset($data['estado']))    $body .= "<p><b>Estado:</b>            " . $data['estado'] . "</p>";
    if(isset($data['unidade']))   $body .= "<p><b>Unidade/Loja:</b>      " . $data['unidade'] . "</p>";
    
    $body .= "<p><b>Mensagem:</b> " . $data['message'] . "</p>";

    $mail->Body = utf8_decode($body);
    $mail->Send();
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();
    
    $_SESSION['success'] = true;
    
    return $app->redirect('trabalhe-conosco-na-concessionaria-jeep-dodge-ram-chrysler-no-rio-de-janeiro-rj');
});



/*
    Custom error handler
*/

$app->post('/enviar_curriculo', function() use($app, $ENDPOINTS){
$data = $_POST;

$job_mail = getRequest($ENDPOINTS['job_email'], $app['debug']);
$client_mail = json_decode($job_mail->job_mail->config);


require("assets/frontend/phpmailer/PHPMailerAutoload.php");

$mail = new PHPMailer(); 
$mail->IsSMTP(); 
$mail->Host = "smtp.gmail.com"; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
$mail->SMTPDebug  = 0;     
$mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
$mail->Port = 587;  	
$mail->Username = 'dealers.reweb@gmail.com'; // Usuário do servidor SMTP (endereço de email)
$mail->Password = 'reweb123'; // Senha do servidor SMTP (senha do email usado)
 
$mail->From = "dealers.reweb@gmail.com"; 
$mail->Sender = "dealers.reweb@gmail.com"; 
$mail->FromName = 'Grupo Euro América | USA Star'; 

// Define os destinatário(s)
$mail->AddAddress('marketing.grupoamericas@gmail.com'); //client_mail
$mail->AddAddress('jaimejunior@euroamericas.com.br'); //client_mail
$mail->AddAddress('rh@italiabarra.com.br'); //client_mail

$mail->IsHTML(true); 

$mail->Subject  = utf8_decode('Envio de currículo'); // Assunto da mensagem

$mail->Body = "
Nome: " . $data['name'] . "<br>
Email: " . $data['email'] . "<br>
Telefone: " . $data['phone'] . "<br>
Mensagem: " . $data['message']					
;
$mail->AddAttachment($_FILES['file']['tmp_name'], $_FILES['file']['name']); 

$mail->Send();

$_SESSION['success'] = true;
// Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();

return $app->redirect('trabalhe-conosco-na-concessionaria-jeep-dodge-ram-chrysler-no-rio-de-janeiro-rj');
//die('ok');
});

$app->error(function (\Exception $e, $code) use($app) {
    if (DEBUG_MODE) {
        return $e;
    }

    $data = defaultData($app);
    $data['title'] = 'Página não encontrada';
    $data['message'] = 'A página solicitada não foi encontrada.';

    //TODO get the pages for the client
    return $app['twig']->render('error.html', $data);

    return new Response('URL não encontrada');
});

$app->run();
